# Project Data

| Title | Tryton updates for Debian |
| ----- | ----- |
| **Funding source** | LTS or Freexian |
| **Submitter** | Jeremiah on behalf of Mathias Behrle <mathiasb@m9s.biz>  |
| **Executor** |  Mathias Behrle |
| **Reviewer** | TBD |
| **Proposal date** | 2021-12-08 |
| **Approval date** | TBD |
| **Request for bids** | (insert link to eventual request) |
| **Project management issue** | (insert link when project starts) |
| **Completion date** | (insert date when completed) |

----------------------------------------

# Rationale

Explain why you are submitting this project. What benefits do you expect
out of its realization?

# Description

Completion of Tryton including productivity improvements

## Project Details

Please see submitter bid section below;

## Completion Criteria

List of one or more specific and measurable criteria that indicate successful
project completion.

* Criterion 1
* Criterion 2

## Estimated Effort

An estimate of the effort involved.  E.g., this project will require up to
8 hours of work. For larger projects, you might want to propose some
intermediary milestones, each with its own work estimation.

This estimation sets expectations for the bids to be submitted and helps
to evaluate the cost of the collected bids.

----------------------------------------

# Submitter Bid

If the submitter wants to be the executor, they can submit their own bid
in this section. Otherwise this section should be dropped.


[Mathias Behrle has] identified several topics to improve the Tryton
ecosystem in Debian:

- Completion of the Tryton suite
  - Packaging of missing published modules of the project
  - Packaging of missing dependencies
  - Packaging of the web client (with e.g. dedicated versioned Javascript
    dependencies provided by tryton.debian.net)
    (This latter issue could BTW be a whole project in itself for Debian wide
    consensus, being talked about at a BoF in DebConf 2016
    ([Pkg-javascript-devel] parallel installation from Date: Mon, 14 Aug 2017).)

- Productivity improvements
  - systemd hardening
  - Packaging and integration of an automatic production grade setup with
    reverse proxy and wsgi backend (could alternatively go into documentation)
    providing several packages (#998319)
  - Full guided setup with debconf and evtl. dbconfig-common 

- User-friendliness of the Tryton suite
  - Easier provision of the Tryton suite
    - Packaging of keys, preferences into a tryton-debian-archive.deb
    - and/or Provide a selector for Debian release and Tryton series (like
      http://mozilla.debian.net/)
    - and/or Integration into Debian main with extrepo

- Debian tooling for the Tryton suite
  - cookiecutter template or script to automate the creation of packages for
    Tryton modules
  - scripting for automatic creation of autopkgtests

- Documentation
  - Documentation of workflow/tools/policy for Debian Tryton Team
  - Documentation of various production setups (single hosts, distributed
    hosts, containers, etc.)

- Completion of the GNUHealth suite
  Note: GNUHealth can currently not go into Debian main because of API
  potential conflicts of the underlying components, it is provided solely by
  tryton.debian.net
  - Packaging of missing published modules etc. of the project
  - Packaging of missing dependencies
  - Integration of the bash-driven maintenance/install/setup concept into Debian
  - Integration with the Tryton setup

