# Proposed checklist for LTS specific rules

Also: https://salsa.debian.org/freexian-team/project-funding/-/blob/master/Rules-LTS.md#how-will-project-proposals-be-approved

This checklist is for moving a project from the idea milestone to the
proposed and then the accepted milestone.

A list of the various milestones might be;

Idea --> proposed --> accepted

Once an idea is moved into "proposed" status we organise a vote among all paid LTS
contributors.
1/ inform the LTS maintainers that you will call for a vote in one week on
whether we accept the project proposal and that they shall ask questions
to the project proposers if they have any
2/ wait one week
3/ organize the vote (maybe with https://framadate.org or other similar
   service, maybe ask the LTS team members if they know of free-software
   friendly services/tools to organize simple polls)"

-[ ] Select a date 
-[ ] Send email to LTS about meeting time to review and vote on
	 proposed project. 
-[ ] Set up date poll to find the best time.

