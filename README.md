:warning: This process is relatively new, it likely has yet unidentified
shortcomings and can probably be improved. Feel free to suggest
changes through merge requests.

# Project Funding

This repository is used to track free software projects that Freexian is
willing to fund to some extent.

Freexian can fund projects for different reasons and the rules applied
in each case will vary depending on the origin of the funding:

* As part of its Debian LTS offering, Freexian has a budget to invest
  into projects that will directly or indirectly improve the Debian
  LTS project. Any Debian member can submit project proposals and they
  will all be considered. See [LTS-specific rules](Rules-LTS.md).

* Freexian can also put out projects proposals on its own. In that case,
  they are usually justified by internal needs or customer requests.
  See [Freexian-specific rules](Rules-Internal.md).
  
# Project Workflow

Each project progresses through several stages.  In order to ensure complete
high quality work on fully vetted projects, a project must be proposed,
approved, executed, and completed.

## Vocabulary

* Project submitter: the person or team that submits a project proposal
* Executor: the person who "executes" or "implements" the project, it's
  the person whose work will be funded by Freexian
* Reviewer: the person who ensures that the executor completed the project
  and that the result is compliant to the project description.
* Bid: an offer made by someone to execute a project at a given price
  following a given timeline
* Project Managers: person(s) appointed by Freexian to handle issues and
  merge requests in this GitLab project. Usually Jeremiah Foster.
* Freexian manager: Raphaël Hertzog or Sébastien Delafond.

## Project Proposal

The project development process begins with the completion of a project proposal
and submission of the proposal.

A sample [project proposal template](https://salsa.debian.org/freexian-team/project-funding/-/blob/master/templates/proposal-instructions.md) and associated instructions for completing
the sections of the project proposal can be found in the _templates/_
sub-directory.  Once the proposal has been prepared and is ready for review, it
should be submitted as a merge request against this project.

It is usually expected that the project submitter also picks the role of
either the reviewer or the executor. When acting as executor, the project
proposal should come with a bid. When acting as reviewer, they should
ideally be ready to review the bids to select the best one.

If you have a project but you want to be neither reviewer nor
executor, then you don't really have a project, you have an idea
and you should likely share it in
[debian/grow-your-ideas](https://salsa.debian.org/debian/grow-your-ideas)
to gauge interest and find persons willing to play (at least) one of those
roles.

### Proposal Naming Convention

Projects (and their associated proposals) are identified using the following
pattern:

> YYYY-MM-short-name.md

Where,

* YYYY - the 4-digit year during which the project proposal draft was submitted
* MM - the 2-digit month during which the project proposal draft was submitted
* short-name - a unique identifier for the project
* .md - the file extension indicating that the proposal is in Markdown format

When you begin drafting your proposal, you may not know the year/month of
initial submission.

The best time to set these values is immediately before committing the proposal
to the _proposed/_ sub-directory for the first time.  Only the person committing
the proposal needs to be concerned with properly assigning the values.

## Project Approval

Project Managers will ensure that pending project proposals are reviewed
regularly (at least once in a semester), assuming that there are
funds to be allocated to new projects.

The review process will follow the rules associated to the funding source
that the project applies to. Currently, only the LTS funding source is
accepting external project submission.

Depending upon the decision, the project proposal will be moved to the
_accepted/_ or _refused/_ sub-directory. It can also be kept in the
_proposed/_ directory, if the project proposal was good but not enough
funding was available to cover all the projects.

Accepted projects can go to the next step. Refused do not progress any
further.

## Project Initialization

To be able to start execution, each project needs both an Executor and a
Reviewer. If there's no Executor, we need to find one by collecting bids
and selecting one of them. If there's no Reviewer, we need to appoint one.

### Collecting Bids to Find an Executor

If the project proposal did not propose anyone to execute the project,
then Project Managers will open a [request for
bids](https://salsa.debian.org/freexian-team/project-funding/-/issues?label_name%5B%5D=Request+for+bids)
and record the issue's URL in the project file. The request for bid will
be open at least 2 weeks.

During that time anyone can submit a bid by filing a new issue. One
may opt to file a confidential issue if they want the details of their
bid to remain private. Or they can use a public issue but only send
the price offer by email to raphael@freexian.com. Issue templates are provided
to aid in the creation of both _Request for Bid_ and _Bid for Proposal_ issues.

The rules to select the winning bid vary depending on the origin of the
funding. Once the winning bid has been selected, Project Managers will
label the associated issue as "Accepted" and close the other bids' issues
with a "Rejected" label.

### Finding a Reviewer

If the project proposal did not define any reviewer, then Freexian must
appoint a reviewer. The reviewer can be a volunteer from the community
or someone paid by Freexian.

## During Project Execution

At the start of the project, Project Managers should create a new issue
labelled with [Project management](https://salsa.debian.org/freexian-team/project-funding/-/issues?label_name%5B%5D=Project+management)
should be created to host all conversations between the Executor and the
Reviewer (there is a dedicated template). The URL of that issue should be
added to the summary table in the project file.

Depending upon the scope of the project and how long execution is planned to
take, intermediary progress reports may be required.

For projects exceeding 10 billable hours or spanning more than 2 weeks, progress
reports are required for each 10 hours of work or every two weeks.

Progress reports should be submitted through the dedicated issue.  Use the
[Progress Report template](templates/Progress Report.md) in this repository to
assist in preparation of progress reports.

If the project incurs cost on a per-hour basis (as opposed to a flat cost for
the project), then approval is required from the Freexian manager before
incurring any costs beyond those agreed upon in the initial proposal.

## After Project Completion

Once the project is complete, a final report should be provided.  As with
progress reports (described above), the final report should be sent in the
dedicated issue.

The Reviewer will review the final report and may request additional
information or clarification.  If any additional work is required, the
Executor will be notified.  An amended final report should be submitted after the
completion of any additional work.

Upon acceptance of the final report, the Reviewer should submit a merge
request filling the completion date in the project file and moving that
file from the _accepted/_ sub-directory to the _completed/_ sub-directory.
The project management issue can be closed in the same operation by adding
a `Fixes #xx` instruction in the commit message.

### Invoicing and Payment

Generally, projects will be paid on a flat fee basis. It is expected that
large projects will be split in multiple steps, each with its own flat fee.

Invoices must be addressed to Freexian (see [administrative
data](https://www.freexian.com/contact/)) and sent by email to
[accounting@freexian.com](mailto:accounting@freexian.com). They will be paid
by wire transfer (or with Paypal if preferred). The email should
include a link to the bid's issue.
