# Project -- 2021-11-security-tracker-database

| Title | Security Tracker Database |
| ----- | ----- |
| **Funding source** | LTS or Freexian |
| **Submitter** | Jeremiah based on communication in the security team and LTS |
| **Executor** |To Be Determined TBD  |
| **Reviewer** | TBD |
| **Proposal date** | 2021-11-16 |
| **Approval date** | NA |
| **Request for bids** | NA |
| **Project management issue** | (insert link when project starts) |
| **Completion date** | (insert date when completed) |

----------------------------------------

# Rationale

Triaging source code based on CVE number can miss code in a library or
in code duplicated in other packages or modules.

# Description

PoC to demonstrate the efficacy of Debian adoption of the osv-schema
standard. This can help map Debian package versions to CVE versions.

## Project Details

This project proposal is aimed at determining if a proof of concept
(PoC) for a database with a schema like the one outlined in
[https://osv.dev/](Open Source Vulnerabilities) and detailed here;
https://ossf.github.io/osv-schema/

A Debian vulnerability database that can interoperate with other
ecosystems (i.e. Golang, Ruby, Rust, NPM, etc.), "would allow common
tools to work across multiple vulnerability databases and simplify the
task of tracking, especially when vulnerabilities touch multiple
languages or subsystems."

## Completion Criteria

* Robust, effective mapping of CVE to all affected Debian packages.

## Estimated Effort

TBD

An estimate of the effort involved.  E.g., this project will require up to
8 hours of work. For larger projects, you might want to propose some
intermediary milestones, each with its own work estimation.

This estimation sets expectations for the bids to be submitted and helps
to evaluate the cost of the collected bids.

----------------------------------------

# Submitter Bid

If the submitter want to be the executor, they can submit their own bid
in this section. Otherwise this section should be dropped.
