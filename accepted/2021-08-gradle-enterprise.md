# Project Data

| Title | Gradle without enterprise plugin |
| ----- | ----- |
| **Funding source** | LTS |
| **Submitter** | debian-android-tools@lists.debian.org, (IRC: #debian-android-tools) |
| **Executor** | To be found |
| **Reviewer** | Phil Morrell <debian@emorrp1.name>, (IRC: emorrp1) |
| **Proposal date** | 2021-08-27 |
| **Approval date** | 2021-09-17 |
| **Request for bids** | (insert link to eventual request) |
| **Project management issue** | (insert link when project starts) |
| **Completion date** | (insert date when completed) |

----------------------------------------

# Rationale

Unblocks packaging effort on gradle, the JVM build tool. There has been no
successful progress for a year.

# Description

Work out how to build Gradle without the gradle-enterprise-gradle-plugin.
https://salsa.debian.org/android-tools-team/admin/-/issues/33

## Project Details

We have Gradle v4.4 in Debian, packaging effort is focussed on v6.4, upstream
is now up to v7.2. The build depends on com.gradle.enterprise, which used to be
an optional module called com.gradle.build-scan in v5. Upstream [asserts that
it's possible] to build without it, but [our initial attempt] was unsuccessful.

Most of the problem comes from massive differences in tooling expectations.
Their [development setup] involves a GUI project tool and an online build,
while debian requires offline builds and chroot management. Therefore while
ideally we'd like a patch to the debian packaging, the bulk of the work
required would be satisfied by a patch to be used in the upstream setup.

It is assumed that if co-ordination with upstream is required, that they will
only respond to work based upon their latest release. If that approach is
taken, then the necessary changes will also need backporting to v6.4.1 so that
we can finish off the debian packaging.

[asserts that it's possible]: https://github.com/gradle/gradle/issues/16439#issuecomment-792580710
[our initial attempt]: https://salsa.debian.org/theloudspeaker-guest/gradle/-/blob/enterprise-test/debian/patches/remove-enterprise.patch
[development setup]: https://github.com/gradle/gradle/blob/master/CONTRIBUTING.md#development-setup

## Completion Criteria

* A patch that applies cleanly to v6.4.1 and builds upstream successfully.
* Some proof that the build does not use or download com.gradle.enterprise.
  e.g. a build log, log diff, jar cache listing or similar
* (Optional) Packaging changes so [the debian build], with kotlin from NEW,
  either succeeds or fails with a non-gradle-enterprise related error.

[the debian build]: https://salsa.debian.org/android-tools-team/admin/-/issues/16#note_235597

## Estimated Effort

Hard to say for this investigation, but I would guess between 10 and 50hrs.
